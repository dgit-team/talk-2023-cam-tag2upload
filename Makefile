
o= >$@.new && mv -f $@.new $@

SLIDES+= intro
SLIDES+= beginning
SLIDES+= lies
SLIDES+= chaos
SLIDES+= t2u
SLIDES+= status
SLIDES+= demo
SLIDES+= status
SLIDES+= t2u

SLIDEFILES=$(addsuffix .ps, $(SLIDES))

all:	slides.pdf talk.ps

slides.ps: $(SLIDEFILES) Makefile
	cat $(SLIDEFILES) $o

%.pdf:     %.ps Makefile
	ps2pdf $< $@

talk.ps: %.ps: %.txt Makefile
	a2ps -1 -f12.5 -o $@.1.tmp -B $<
	pstops <$@.1.tmp >$@ '0@0.94(7mm,7.5mm)'

%.ps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4  <$@.1 $o

beginning.ps:   lies.fig Makefile
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 -D -20  <$@.1 $o
